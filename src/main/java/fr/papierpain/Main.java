package fr.papierpain;

public class Main {
    private final String message = "Coucou !";

    public Main() {}

    public static void main(String[] args) {
        System.out.println(new Main().getMessage());
    }

    public final String getMessage() {
        return message;
    }
}
