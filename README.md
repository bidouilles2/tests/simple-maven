# Sample Maven

## Description

Simple projet Java en Maven.

## Release note

### Version 1.0.0

Début de la release : 13.08.2022

| Type  | Ticket | Sujet                            |
| ----- | ------ | -------------------------------- |
| Story | PAP-81 | Remplacer Hello World par Coucou |
| Story | PAP-80 | Création d'un projet Java Maven  |
